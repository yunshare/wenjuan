export const baseUrl="https://cjyc.beautop.cn/tuoban/api/"

// 封装请求函数
export function postRequest(url, data) {
  return new Promise((resolve, reject) => {
	  let token=""
	  uni.getStorage({
	    key: 'token',
	  	  success: function (res) { 
	  		token=res.data
	  	  } 
	    }) 
		
     uni.request({
       url: baseUrl+url,
       method: 'POST',
       data: data,
       header: {
         'token': token
       },
       success: function (res) {
         resolve(res.data);
       },
       fail: function (err) {
         reject(err);
       }
     });
   });
}