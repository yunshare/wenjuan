import wxjs from "weixin-js-sdk";
/**   
 * 分享封装
 * 一般正常情况下，都会出现一些问题，例如签名错误啊之类的，下面是官网文档，有你想要的答案
 * 官方文档地址： https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html#1
 * configData 微信配置文件 包括appid 、timestamp、nonceStr、signature ；这些都需要后端人员传给你
 * list 分享数据 包括（title、desc、imgUrl）共三个参数足够，分别时 标题、描述与图片
 */
export function myshare(configData, list) {
	let link = location.href // 获取当前页面的地址参数
	// 必须先配置参数，这些参数需要用到公众号，让后端去拿 然后前端请求数据即可
	// console.log(configData)
	wxjs.config({
		debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
		appId: configData.appId, // 必填，公众号的唯一标识
		timestamp: configData.timestamp, // 必填，生成签名的时间戳
		nonceStr: configData.nonceStr, // 必填，生成签名的随机串
		signature: configData.signature, // 必填，签名
		jsApiList: ['updateAppMessageShareData', 'updateTimelineShareData'] // 必填，需要使用的JS接口列表
	});
	
	// 需在用户可能点击分享按钮前就先调用 初始化分享事件
	wxjs.ready(function() { 
		// 自定义“分享给朋友”及“分享到QQ”按钮的分享内容（1.4.0）
		wxjs.updateAppMessageShareData({
			title: list.title, // 分享标题
			desc: list.desc, // 分享描述
			link: list.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
			imgUrl: list.imgUrl, // 分享图标
			success: function(res) {
				// console.log(res.data)
				// alert("分享成功1")
				// 设置成功
			},
			fail: function(res) {
				// alert("分享失败1")
				// console.log(res)
				// alert(JSON.stringify(res));
			},
			cancel: function() {
				// 用户取消分享后执行的回调函数
				// alert("用户取消分享1")
			}
		});



		// 自定义“分享到朋友圈”及“分享到QQ空间”按钮的分享内容（1.4.0）
		wxjs.updateTimelineShareData({
			title: list.title, // 分享标题
			desc: list.desc, // 分享描述
			link: "https://cjyc.beautop.cn/app/zyjy/index.html", // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
			imgUrl: list.imgUrl, // 分享图标
			success: function(res) {
				// console.log(res.data)
				// alert("分享成功")
				// 设置成功
			},
			fail: function(res) {
				// alert("分享失败")
				// console.log(res)
				// alert(JSON.stringify(res));
			},
			cancel: function() {
				// 用户取消分享后执行的回调函数	 
				// alert("用户取消分享")
			}
		})

	});

}
