export const chuzhong_json_demo = [
	{
		"pkId": "2",
		"title": "中学生心理健康诊断测验",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度一</span>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
		"number": 20,
		"questions": [
			{
				"questionId": 'school',
				"questionType": "SELECT",
				"title": "选择学校",
				"group": 0, 
				"children": []
			},
			{
				"questionId": 'stu_grade',
				"questionType": "SELECT",
				"title": "选择年级",
				"group": 0, 
				"children": []
			}, {
				"questionId": 'stu_class',
				"questionType": "SELECT",
				"title": "选择班级",
				"group": 0, 
				"children": []
			},
			{
				"questionId": 'name',
				"questionType": 'QUESTION_INPUT',
				"title": '姓名',
				"group": 0,
				"children": []
			},
			{
				"questionId": 'tel',
				"questionType": 'QUESTION_INPUT',
				"title": '电话',
				"group": 0,
				"children": []
			},

			{
				"questionId": 1,
				"questionType": "SINGLE",
				"title": "你夜里睡觉时，是否总想着明天的功课？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 2,
				"questionType": "SINGLE",
				"title": "老师在向全班提问时，你是否会觉得是在提问自己而感到不安？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 3,
				"questionType": "SINGLE",
				"title": "你是否一听说“要考试”心里就紧张。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 4,
				"questionType": "SINGLE",
				"title": "你考试成绩不好时，心里是否感到不快。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 5,
				"questionType": "SINGLE",
				"title": "你学习成绩不好时，是否总是提心吊胆。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 6,
				"questionType": "SINGLE",
				"title": "考试时，当你想不起来原先掌握的知识时，你是否会感到焦虑？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 7,
				"questionType": "SINGLE",
				"title": "你考试后，在没有知道成绩之前，是否总是放心不下？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 8,
				"questionType": "SINGLE",
				"title": "你是否一遇到考试，就担心会考坏？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 9,
				"questionType": "SINGLE",
				"title": "你是否希望考试能顺利通过？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 10,
				"questionType": "SINGLE",
				"title": "你在没有完成任务之前，是否总担心完不成任务？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 11,
				"questionType": "SINGLE",
				"title": "你当着大家的面朗读课文时，是否总是怕读错？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 12,
				"questionType": "SINGLE",
				"title": "是否认为学校里得到的学习成绩总是不大可靠的？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 13,
				"questionType": "SINGLE",
				"title": "你是否认为你比别人更担心学习？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 14,
				"questionType": "SINGLE",
				"title": "你是否做过考试考坏了的梦？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 15,
				"questionType": "SINGLE",
				"title": "你是否做过学习成绩不好时，受到爸爸妈妈或老师训斥的梦？",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
		]
	},
	{
		"pkId": "2",
		"title": "中学生心理健康诊断测验",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度二</span>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
		"number": 10,
		"questions": [
			{
				"questionId": 16,
				"questionType": "SINGLE",
				"title": "你是否经常觉得有同学在背后说你的坏话？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 17,
				"questionType": "SINGLE",
				"title": "你受到父母批评后，是否总是想不开，放在心上？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 18,
				"questionType": "SINGLE",
				"title": "你在游戏或与别人的竞争中输给了对方，是否就不想再干了？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 19,
				"questionType": "SINGLE",
				"title": "人家在背后议论你，你是否感到讨厌？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 20,
				"questionType": "SINGLE",
				"title": "你在大家面前或被老师提问时，是否会脸红？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 21,
				"questionType": "SINGLE",
				"title": "你是否很担心叫你担任班干部？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 22,
				"questionType": "SINGLE",
				"title": "你是否总是觉得好像有人在注意你？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 23,
				"questionType": "SINGLE",
				"title": "在工作或学习时，如果有人注意你，你心里是否紧张？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 24,
				"questionType": "SINGLE",
				"title": "你受到批评时，心情是否不愉快？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 25,
				"questionType": "SINGLE",
				"title": "你受到老师批评时，心里是否总是不安？",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "2",
		"title": "中学生心理健康诊断测验",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度三</span>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
		"number": 10,
		"questions": [
			{
				"questionId": 26,
				"questionType": "SINGLE",
				"title": "同学们在笑时，你是否也不大会笑？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 27,
				"questionType": "SINGLE",
				"title": "你是否觉得到同学家里去玩不如在自己家里玩？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 28,
				"questionType": "SINGLE",
				"title": "你和大家在一起时，是否也觉得自己是孤单的一个人？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 29,
				"questionType": "SINGLE",
				"title": "你是否觉得和同学一起玩，不如自己一个人玩？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 30,
				"questionType": "SINGLE",
				"title": "同学们在交谈时，你是否不想加入？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 31,
				"questionType": "SINGLE",
				"title": "当你和大家在一起时，是否觉得自己是多余的人？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 32,
				"questionType": "SINGLE",
				"title": "你是否讨厌参加运动会和文艺演出会？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 33,
				"questionType": "SINGLE",
				"title": "你的朋友是否很少？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 34,
				"questionType": "SINGLE",
				"title": "你是否不喜欢同别人谈话？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 35,
				"questionType": "SINGLE",
				"title": "在人多的地方，你是否觉得很怕？",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "2",
		"title": "中学生心理健康诊断测验",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度四</span>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
		"number": 10,
		"questions": [
			{
				"questionId": 36,
				"questionType": "SINGLE",
				"title": "你在排球、篮球、足球、拔河、广播操等体育比赛输了时，心里是否一直认为自己不好？",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 37,
				"questionType": "SINGLE",
				"title": "你受到批评后，是否总认为是自己不好？",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 38,
				"questionType": "SINGLE",
				"title": "别人笑你的时候，你是否会认为是自己做错了什么事？",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 39,
				"questionType": "SINGLE",
				"title": "你学习成绩不好时，是否总是认为是自己不用功的缘故？",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 40,
				"questionType": "SINGLE",
				"title": "你失败的时候，是否总是认为是自己的责任？",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 41,
				"questionType": "SINGLE",
				"title": "大家受到责备时，你是否认为主要是自己的过错？",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 42,
				"questionType": "SINGLE",
				"title": "你在乒乓球、羽毛球、篮球、足球、拔河、广播操等体育比赛时，是否一出错就特别留神。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 43,
				"questionType": "SINGLE",
				"title": "碰到为难的事情时，你是否认为自己难以应付？",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 44,
				"questionType": "SINGLE",
				"title": "你是否有时会后悔，那件事不做就好了？",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 45,
				"questionType": "SINGLE",
				"title": "你和同学吵架以后，是否总是认为是自己的错？",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "2",
		"title": "中学生心理健康诊断测验",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度五</span>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
		"number": 10,
		"questions": [
			{
				"questionId": 46,
				"questionType": "SINGLE",
				"title": "你心里是否总想为班级做点好事？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 47,
				"questionType": "SINGLE",
				"title": "你学习的时候，思想是否经常开小差？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 48,
				"questionType": "SINGLE",
				"title": "你把东西借给别人时，是否担心别人会把东西弄坏？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 49,
				"questionType": "SINGLE",
				"title": "碰到不顺利的事情时，你心里是否很烦躁？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 50,
				"questionType": "SINGLE",
				"title": "你是否非常担心家里有人生病或死去？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 51,
				"questionType": "SINGLE",
				"title": "你是否在梦里见到过死去的人？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 52,
				"questionType": "SINGLE",
				"title": "你对收音机和汽车的声音是否特别敏感？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 53,
				"questionType": "SINGLE",
				"title": "你心里是否总觉得好像有什么事没有做好？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 54,
				"questionType": "SINGLE",
				"title": "你是否担心会发生什么意外的事？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 55,
				"questionType": "SINGLE",
				"title": "你在决定要做什么事时，是否总是犹豫不决？",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "2",
		"title": "中学生心理健康诊断测验",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度六</span>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
		"number": 15,
		"questions": [
			{
				"questionId": 56,
				"questionType": "SINGLE",
				"title": "你手上是否经常出汗？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 57,
				"questionType": "SINGLE",
				"title": "你害羞时是否会脸红？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 58,
				"questionType": "SINGLE",
				"title": "你是否经常头痛？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 59,
				"questionType": "SINGLE",
				"title": "你被老师提问时，心里是否总是很紧张？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 60,
				"questionType": "SINGLE",
				"title": "你没有参加运动，心脏是否经常噗通噗通地跳？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 61,
				"questionType": "SINGLE",
				"title": "你是否很容易疲劳？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 62,
				"questionType": "SINGLE",
				"title": "你是否很不愿吃药？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 63,
				"questionType": "SINGLE",
				"title": "夜里你是否很难入睡？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 64,
				"questionType": "SINGLE",
				"title": "你是否总觉得身体好像有什么毛病？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 65,
				"questionType": "SINGLE",
				"title": "你是否经常认为自己的体型和面孔比别人难看？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 66,
				"questionType": "SINGLE",
				"title": "你是否经常觉得肠胃不好？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 67,
				"questionType": "SINGLE",
				"title": "你是否经常咬指甲？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 68,
				"questionType": "SINGLE",
				"title": "你是否舔手指头？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 69,
				"questionType": "SINGLE",
				"title": "你是否经常感到呼吸困难？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 70,
				"questionType": "SINGLE",
				"title": "你去厕所的次数是否比别人多？",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "2",
		"title": "中学生心理健康诊断测验",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度七</span>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
		"number": 10,
		"questions": [
			{
				"questionId": 71,
				"questionType": "SINGLE",
				"title": "是否很怕到高的地方去？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 72,
				"questionType": "SINGLE",
				"title": "你是否害怕很多东西？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 73,
				"questionType": "SINGLE",
				"title": "你是否经常做噩梦？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 74,
				"questionType": "SINGLE",
				"title": "你胆子是否很小？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 75,
				"questionType": "SINGLE",
				"title": "夜里，你是否很怕一个人在房间里睡觉？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 76,
				"questionType": "SINGLE",
				"title": "你乘车穿过隧道或路过高桥时，是否很怕？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 77,
				"questionType": "SINGLE",
				"title": "你是否喜欢整夜开着灯睡觉？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 78,
				"questionType": "SINGLE",
				"title": "你听到打雷声是否非常害怕？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 79,
				"questionType": "SINGLE",
				"title": "你是否非常害怕黑暗？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 80,
				"questionType": "SINGLE",
				"title": "你是否经常感到后面有人跟着你？",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "2",
		"title": "中学生心理健康诊断测验",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度八,维度九</span>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
		"number": 20,
		"questions": [
			{
				"questionId": 81,
				"questionType": "SINGLE",
				"title": "你是否经常生气？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 82,
				"questionType": "SINGLE",
				"title": "你是否不想得到好的成绩？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 83,
				"questionType": "SINGLE",
				"title": "你是否经常会突然想哭？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 84,
				"questionType": "SINGLE",
				"title": "你以前是否说过谎话？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 85,
				"questionType": "SINGLE",
				"title": "你有时是否会觉得，还是死了好？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 86,
				"questionType": "SINGLE",
				"title": "你是否一次也没有失约过？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 87,
				"questionType": "SINGLE",
				"title": "你是否经常想大声喊叫？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 88,
				"questionType": "SINGLE",
				"title": "你是否不愿说出别人不让说的事？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 89,
				"questionType": "SINGLE",
				"title": "你有时是否想过自己一个人到遥远的地方去？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 90,
				"questionType": "SINGLE",
				"title": "你是否总是很有礼貌？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 91,
				"questionType": "SINGLE",
				"title": "你被人说了坏话，是否想立即采取报复行动？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 92,
				"questionType": "SINGLE",
				"title": "老师或父母说的话，你是否都照办？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 93,
				"questionType": "SINGLE",
				"title": "你心里不开心，是否会乱丢、乱砸东西？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 94,
				"questionType": "SINGLE",
				"title": "你是否发过怒？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 95,
				"questionType": "SINGLE",
				"title": "你想要的东西，是否就一定要拿到手？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 96,
				"questionType": "SINGLE",
				"title": "你不喜欢的课，老师提前下课，你是否会感到特别高兴？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 97,
				"questionType": "SINGLE",
				"title": "你是否经常想从高的地方跳下来？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 98,
				"questionType": "SINGLE",
				"title": "你是否无论对谁都很亲热？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 99,
				"questionType": "SINGLE",
				"title": "你是否会经常急躁得坐立不安？",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			},
			{
				"questionId": 100,
				"questionType": "SINGLE",
				"title": "对不认识的人，你是否会都喜欢？",
				"group": 9,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "是",
						"score": 1
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "不是",
						"score": 0
					}
				]
			}
		]
	}

]


export const xiaoxue_json_demo = [
	{
		"pkId": "1",
		"title": "小学生心理健康评定量表",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度一</span><br/>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>",
		"number": 15,
		"questions": [
			{
				"questionId": 'school',
				"questionType": "SELECT",
				"title": "选择学校",
				"group": 0, 
				"children": []
			},
			{
				"questionId": 'stu_grade',
				"questionType": "SELECT",
				"title": "选择年级",
				"group": 0, 
				"children": []
			}, {
				"questionId": 'stu_class',
				"questionType": "SELECT",
				"title": "选择班级",
				"group": 0, 
				"children": []
			},
			{
				"questionId": 'name',
				"questionType": 'QUESTION_INPUT',
				"title": '姓名',
				"group": 0,
				"children": []
			},
			{
				"questionId": 'tel',
				"questionType": 'QUESTION_INPUT',
				"title": '电话',
				"group": 0,
				"children": []
			}, 
			{
				"questionId": 1,
				"questionType": "SINGLE",
				"title": "不能正确认识字母或拼读音节。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 2,
				"questionType": "SINGLE",
				"title": "不能正确辨认汉字。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 3,
				"questionType": "SINGLE",
				"title": "不懂得数的大小和序列关系。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 4,
				"questionType": "SINGLE",
				"title": "计算困难。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 5,
				"questionType": "SINGLE",
				"title": "绘画时定位不准，涂色不合规范。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 6,
				"questionType": "SINGLE",
				"title": "图画作品中有前后左右位置颠倒的现象。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 7,
				"questionType": "SINGLE",
				"title": "一提起学习即心烦意乱。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 8,
				"questionType": "SINGLE",
				"title": "课堂讨论或与家长谈论学习问题时不感兴趣。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 9,
				"questionType": "SINGLE",
				"title": "不能按时交作业或作业质量差。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 10,
				"questionType": "SINGLE",
				"title": "考试不及格。",
				"group": 1,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "1",
		"title": "小学生心理健康评定量表",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度二</span><br/>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>",
		"number": 10,
		"questions": [
			{
				"questionId": 11,
				"questionType": "SINGLE",
				"title": "遇到一点小事也担忧。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 12,
				"questionType": "SINGLE",
				"title": "心神不定，坐立不安。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 13,
				"questionType": "SINGLE",
				"title": "食欲不振，心慌气促。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 14,
				"questionType": "SINGLE",
				"title": "头痛失眠汗多尿频。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 15,
				"questionType": "SINGLE",
				"title": "害怕上学，多方逃避。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 16,
				"questionType": "SINGLE",
				"title": "不敢独自出家门。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 17,
				"questionType": "SINGLE",
				"title": "一人独处时恐慌害怕。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 18,
				"questionType": "SINGLE",
				"title": "无缘无故地闷闷不乐。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 19,
				"questionType": "SINGLE",
				"title": "精力下降，活动减少。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 20,
				"questionType": "SINGLE",
				"title": "受到重大刺激不激动不流泪。",
				"group": 2,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "1",
		"title": "小学生心理健康评定量表",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度三</span><br/>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>",
		"number": 10,
		"questions": [
			{
				"questionId": 21,
				"questionType": "SINGLE",
				"title": "心胸狭窄，猜疑。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 22,
				"questionType": "SINGLE",
				"title": "依赖他人。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 23,
				"questionType": "SINGLE",
				"title": "嫉妒他人。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 24,
				"questionType": "SINGLE",
				"title": "胆怯，害羞。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 25,
				"questionType": "SINGLE",
				"title": "自卑自责。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 26,
				"questionType": "SINGLE",
				"title": "遇事犹豫不决。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 27,
				"questionType": "SINGLE",
				"title": "固执任性。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 28,
				"questionType": "SINGLE",
				"title": "容易发火。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 29,
				"questionType": "SINGLE",
				"title": "孤僻不合群。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 30,
				"questionType": "SINGLE",
				"title": "与人对立。",
				"group": 3,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "1",
		"title": "小学生心理健康评定量表",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度四</span><br/>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>",
		"number": 10,
		"questions": [
			{
				"questionId": 31,
				"questionType": "SINGLE",
				"title": "交新朋友困难。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 32,
				"questionType": "SINGLE",
				"title": "在集体场合适应困难。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 33,
				"questionType": "SINGLE",
				"title": "自我中心，不遵守集体规则。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 34,
				"questionType": "SINGLE",
				"title": "不能融洽地与同学相处。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 35,
				"questionType": "SINGLE",
				"title": "与教师或家长发生冲突。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 36,
				"questionType": "SINGLE",
				"title": "被别人误解后耿耿于怀。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 37,
				"questionType": "SINGLE",
				"title": "不能和常人一样地与异性交往。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 38,
				"questionType": "SINGLE",
				"title": "受到挫折后反应过分强烈或压抑。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 39,
				"questionType": "SINGLE",
				"title": "容易闯祸。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 40,
				"questionType": "SINGLE",
				"title": "面对新环境（迁居转学等）适应困难。",
				"group": 4,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "1",
		"title": "小学生心理健康评定量表",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度五</span><br/>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>",
		"number": 10,
		"questions": [
			{
				"questionId": 41,
				"questionType": "SINGLE",
				"title": "骂人。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 42,
				"questionType": "SINGLE",
				"title": "搞恶作剧。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 43,
				"questionType": "SINGLE",
				"title": "起哄，无理取闹。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 44,
				"questionType": "SINGLE",
				"title": "打架斗殴。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 45,
				"questionType": "SINGLE",
				"title": "故意破坏。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 46,
				"questionType": "SINGLE",
				"title": "考试作弊。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 47,
				"questionType": "SINGLE",
				"title": "说谎。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 48,
				"questionType": "SINGLE",
				"title": "偷盗。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 49,
				"questionType": "SINGLE",
				"title": "逃学。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 50,
				"questionType": "SINGLE",
				"title": "离家出走。",
				"group": 5,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "1",
		"title": "小学生心理健康评定量表",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度六</span><br/>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>",
		"number": 10,
		"questions": [
			{
				"questionId": 51,
				"questionType": "SINGLE",
				"title": "习惯性眨眼。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 52,
				"questionType": "SINGLE",
				"title": "习惯性皱眉或皱额。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 53,
				"questionType": "SINGLE",
				"title": "习惯性努嘴或嗅鼻。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 54,
				"questionType": "SINGLE",
				"title": "习惯性点头或摇头。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 55,
				"questionType": "SINGLE",
				"title": "习惯性吞咽或打呃。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 56,
				"questionType": "SINGLE",
				"title": "习惯性咳嗽。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 57,
				"questionType": "SINGLE",
				"title": "习惯性耸肩。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 58,
				"questionType": "SINGLE",
				"title": "吸吮手指嘴嚼衣服或其他物品。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 59,
				"questionType": "SINGLE",
				"title": "咬指甲。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 60,
				"questionType": "SINGLE",
				"title": "吸烟或饮酒。",
				"group": 6,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "1",
		"title": "小学生心理健康评定量表",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度七</span><br/>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>",
		"number": 10,
		"questions": [
			{
				"questionId": 61,
				"questionType": "SINGLE",
				"title": "反复数课本或其他图书上人物的数目。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 62,
				"questionType": "SINGLE",
				"title": "反复检查作业是否作对了。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 63,
				"questionType": "SINGLE",
				"title": "睡觉前反复检查个人的衣服鞋袜是否放整齐了。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 64,
				"questionType": "SINGLE",
				"title": "一天洗手十几次，每次持续十几分钟。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 65,
				"questionType": "SINGLE",
				"title": "注意力不集中，做事有头无尾。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 66,
				"questionType": "SINGLE",
				"title": "上课时小动作多，干扰他人。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 67,
				"questionType": "SINGLE",
				"title": "不合场合，特别好动。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 68,
				"questionType": "SINGLE",
				"title": "做作业时边做边玩。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 69,
				"questionType": "SINGLE",
				"title": "好冲动行动鲁莽。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 70,
				"questionType": "SINGLE",
				"title": "不知危险，好伤人或自伤。",
				"group": 7,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			}
		]
	},
	{
		"pkId": "1",
		"title": "小学生心理健康评定量表",
		"desc": "<p><span style='color:#1e80ff;font-weight:600'>维度八</span><br/>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>",
		"number": 10,
		"questions": [
			{
				"questionId": 71,
				"questionType": "SINGLE",
				"title": "尿床。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 72,
				"questionType": "SINGLE",
				"title": "口吃。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 73,
				"questionType": "SINGLE",
				"title": "好沉默不语，甚至长时间一言不发。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 74,
				"questionType": "SINGLE",
				"title": "入睡困难。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 75,
				"questionType": "SINGLE",
				"title": "睡觉不安稳，好讲梦话。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 76,
				"questionType": "SINGLE",
				"title": "睡觉时好磨牙。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 77,
				"questionType": "SINGLE",
				"title": "睡觉中突然哭喊惊叫。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 78,
				"questionType": "SINGLE",
				"title": "睡觉中突然起床活动，醒后对此无记忆。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 79,
				"questionType": "SINGLE",
				"title": "厌食偏食或拒食。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			},
			{
				"questionId": 80,
				"questionType": "SINGLE",
				"title": "身体无病却反复呕吐。",
				"group": 8,
				"children": [
					{
						"id": "A",
						"state": 0,
						"serial": "A",
						"content": "经常",
						"score": 2
					},
					{
						"id": "B",
						"state": 0,
						"serial": "B",
						"content": "偶尔",
						"score": 1
					},
					{
						"id": "C",
						"state": 0,
						"serial": "C",
						"content": "没有",
						"score": 0
					}
				]
			}
		]
	},
]
