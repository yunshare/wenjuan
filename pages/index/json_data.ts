// export const chuzhong_json ={
//   "pkId": "2",
//   "title": "中学生心理健康诊断测验(MHT)",
//   "desc": "<p>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
//   "number": 100,
//   "questions":
  
// }


export const chuzhong_json ={
  "pkId": "2",
  "title": "中学生心理健康诊断测验",
  "desc": "<p>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
  "number": 100,
  "is_sum":true, 
  "sum_key":'10',
  "questions":
  [
    {
      "questionId": 1,
      "questionType": "SINGLE",
      "title": "你夜里睡觉时，是否总想着明天的功课？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 2,
      "questionType": "SINGLE",
      "title": "老师在向全班提问时，你是否会觉得是在提问自己而感到不安？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 3,
      "questionType": "SINGLE",
      "title": "你是否一听说“要考试”心里就紧张。",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 4,
      "questionType": "SINGLE",
      "title": "你考试成绩不好时，心里是否感到不快。",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 5,
      "questionType": "SINGLE",
      "title": "你学习成绩不好时，是否总是提心吊胆。",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 6,
      "questionType": "SINGLE",
      "title": "考试时，当你想不起来原先掌握的知识时，你是否会感到焦虑？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 7,
      "questionType": "SINGLE",
      "title": "你考试后，在没有知道成绩之前，是否总是放心不下？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 8,
      "questionType": "SINGLE",
      "title": "你是否一遇到考试，就担心会考坏？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 9,
      "questionType": "SINGLE",
      "title": "你是否希望考试能顺利通过？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 10,
      "questionType": "SINGLE",
      "title": "你在没有完成任务之前，是否总担心完不成任务？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 11,
      "questionType": "SINGLE",
      "title": "你当着大家的面朗读课文时，是否总是怕读错？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 12,
      "questionType": "SINGLE",
      "title": "是否认为学校里得到的学习成绩总是不大可靠的？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 13,
      "questionType": "SINGLE",
      "title": "你是否认为你比别人更担心学习？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 14,
      "questionType": "SINGLE",
      "title": "你是否做过考试考坏了的梦？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 15,
      "questionType": "SINGLE",
      "title": "你是否做过学习成绩不好时，受到爸爸妈妈或老师训斥的梦？",
      "group": 1,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 16,
      "questionType": "SINGLE",
      "title": "你是否经常觉得有同学在背后说你的坏话？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 17,
      "questionType": "SINGLE",
      "title": "你受到父母批评后，是否总是想不开，放在心上？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 18,
      "questionType": "SINGLE",
      "title": "你在游戏或与别人的竞争中输给了对方，是否就不想再干了？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 19,
      "questionType": "SINGLE",
      "title": "人家在背后议论你，你是否感到讨厌？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 20,
      "questionType": "SINGLE",
      "title": "你在大家面前或被老师提问时，是否会脸红？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 21,
      "questionType": "SINGLE",
      "title": "你是否很担心叫你担任班干部？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 22,
      "questionType": "SINGLE",
      "title": "你是否总是觉得好像有人在注意你？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 23,
      "questionType": "SINGLE",
      "title": "在工作或学习时，如果有人注意你，你心里是否紧张？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 24,
      "questionType": "SINGLE",
      "title": "你受到批评时，心情是否不愉快？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 25,
      "questionType": "SINGLE",
      "title": "你受到老师批评时，心里是否总是不安？",
      "group": 2,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 26,
      "questionType": "SINGLE",
      "title": "同学们在笑时，你是否也不大会笑？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 27,
      "questionType": "SINGLE",
      "title": "你是否觉得到同学家里去玩不如在自己家里玩？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 28,
      "questionType": "SINGLE",
      "title": "你和大家在一起时，是否也觉得自己是孤单的一个人？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 29,
      "questionType": "SINGLE",
      "title": "你是否觉得和同学一起玩，不如自己一个人玩？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 30,
      "questionType": "SINGLE",
      "title": "同学们在交谈时，你是否不想加入？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 31,
      "questionType": "SINGLE",
      "title": "当你和大家在一起时，是否觉得自己是多余的人？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 32,
      "questionType": "SINGLE",
      "title": "你是否讨厌参加运动会和文艺演出会？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 33,
      "questionType": "SINGLE",
      "title": "你的朋友是否很少？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 34,
      "questionType": "SINGLE",
      "title": "你是否不喜欢同别人谈话？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 35,
      "questionType": "SINGLE",
      "title": "在人多的地方，你是否觉得很怕？",
      "group": 3,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 36,
      "questionType": "SINGLE",
      "title": "你在排球、篮球、足球、拔河、广播操等体育比赛输了时，心里是否一直认为自己不好？",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 37,
      "questionType": "SINGLE",
      "title": "你受到批评后，是否总认为是自己不好？",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 38,
      "questionType": "SINGLE",
      "title": "别人笑你的时候，你是否会认为是自己做错了什么事？",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 39,
      "questionType": "SINGLE",
      "title": "你学习成绩不好时，是否总是认为是自己不用功的缘故？",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 40,
      "questionType": "SINGLE",
      "title": "你失败的时候，是否总是认为是自己的责任？",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 41,
      "questionType": "SINGLE",
      "title": "大家受到责备时，你是否认为主要是自己的过错？",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 42,
      "questionType": "SINGLE",
      "title": "你在乒乓球、羽毛球、篮球、足球、拔河、广播操等体育比赛时，是否一出错就特别留神。",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 43,
      "questionType": "SINGLE",
      "title": "碰到为难的事情时，你是否认为自己难以应付？",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 44,
      "questionType": "SINGLE",
      "title": "你是否有时会后悔，那件事不做就好了？",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 45,
      "questionType": "SINGLE",
      "title": "你和同学吵架以后，是否总是认为是自己的错？",
      "group": 4,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 46,
      "questionType": "SINGLE",
      "title": "你心里是否总想为班级做点好事？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 47,
      "questionType": "SINGLE",
      "title": "你学习的时候，思想是否经常开小差？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 48,
      "questionType": "SINGLE",
      "title": "你把东西借给别人时，是否担心别人会把东西弄坏？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 49,
      "questionType": "SINGLE",
      "title": "碰到不顺利的事情时，你心里是否很烦躁？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 50,
      "questionType": "SINGLE",
      "title": "你是否非常担心家里有人生病或死去？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 51,
      "questionType": "SINGLE",
      "title": "你是否在梦里见到过死去的人？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 52,
      "questionType": "SINGLE",
      "title": "你对收音机和汽车的声音是否特别敏感？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 53,
      "questionType": "SINGLE",
      "title": "你心里是否总觉得好像有什么事没有做好？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 54,
      "questionType": "SINGLE",
      "title": "你是否担心会发生什么意外的事？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 55,
      "questionType": "SINGLE",
      "title": "你在决定要做什么事时，是否总是犹豫不决？",
      "group": 5,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 56,
      "questionType": "SINGLE",
      "title": "你手上是否经常出汗？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 57,
      "questionType": "SINGLE",
      "title": "你害羞时是否会脸红？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 58,
      "questionType": "SINGLE",
      "title": "你是否经常头痛？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 59,
      "questionType": "SINGLE",
      "title": "你被老师提问时，心里是否总是很紧张？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 60,
      "questionType": "SINGLE",
      "title": "你没有参加运动，心脏是否经常噗通噗通地跳？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 61,
      "questionType": "SINGLE",
      "title": "你是否很容易疲劳？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 62,
      "questionType": "SINGLE",
      "title": "你是否很不愿吃药？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 63,
      "questionType": "SINGLE",
      "title": "夜里你是否很难入睡？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 64,
      "questionType": "SINGLE",
      "title": "你是否总觉得身体好像有什么毛病？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 65,
      "questionType": "SINGLE",
      "title": "你是否经常认为自己的体型和面孔比别人难看？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 66,
      "questionType": "SINGLE",
      "title": "你是否经常觉得肠胃不好？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 67,
      "questionType": "SINGLE",
      "title": "你是否经常咬指甲？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 68,
      "questionType": "SINGLE",
      "title": "你是否舔手指头？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 69,
      "questionType": "SINGLE",
      "title": "你是否经常感到呼吸困难？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 70,
      "questionType": "SINGLE",
      "title": "你去厕所的次数是否比别人多？",
      "group": 6,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 71,
      "questionType": "SINGLE",
      "title": "是否很怕到高的地方去？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 72,
      "questionType": "SINGLE",
      "title": "你是否害怕很多东西？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 73,
      "questionType": "SINGLE",
      "title": "你是否经常做噩梦？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 74,
      "questionType": "SINGLE",
      "title": "你胆子是否很小？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 75,
      "questionType": "SINGLE",
      "title": "夜里，你是否很怕一个人在房间里睡觉？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 76,
      "questionType": "SINGLE",
      "title": "你乘车穿过隧道或路过高桥时，是否很怕？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 77,
      "questionType": "SINGLE",
      "title": "你是否喜欢整夜开着灯睡觉？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 78,
      "questionType": "SINGLE",
      "title": "你听到打雷声是否非常害怕？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 79,
      "questionType": "SINGLE",
      "title": "你是否非常害怕黑暗？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 80,
      "questionType": "SINGLE",
      "title": "你是否经常感到后面有人跟着你？",
      "group": 7,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 81,
      "questionType": "SINGLE",
      "title": "你是否经常生气？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 82,
      "questionType": "SINGLE",
      "title": "你是否不想得到好的成绩？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 83,
      "questionType": "SINGLE",
      "title": "你是否经常会突然想哭？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 84,
      "questionType": "SINGLE",
      "title": "你以前是否说过谎话？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 85,
      "questionType": "SINGLE",
      "title": "你有时是否会觉得，还是死了好？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 86,
      "questionType": "SINGLE",
      "title": "你是否一次也没有失约过？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 87,
      "questionType": "SINGLE",
      "title": "你是否经常想大声喊叫？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 88,
      "questionType": "SINGLE",
      "title": "你是否不愿说出别人不让说的事？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 89,
      "questionType": "SINGLE",
      "title": "你有时是否想过自己一个人到遥远的地方去？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 90,
      "questionType": "SINGLE",
      "title": "你是否总是很有礼貌？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 91,
      "questionType": "SINGLE",
      "title": "你被人说了坏话，是否想立即采取报复行动？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 92,
      "questionType": "SINGLE",
      "title": "老师或父母说的话，你是否都照办？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 93,
      "questionType": "SINGLE",
      "title": "你心里不开心，是否会乱丢、乱砸东西？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 94,
      "questionType": "SINGLE",
      "title": "你是否发过怒？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 95,
      "questionType": "SINGLE",
      "title": "你想要的东西，是否就一定要拿到手？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 96,
      "questionType": "SINGLE",
      "title": "你不喜欢的课，老师提前下课，你是否会感到特别高兴？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 97,
      "questionType": "SINGLE",
      "title": "你是否经常想从高的地方跳下来？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 98,
      "questionType": "SINGLE",
      "title": "你是否无论对谁都很亲热？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 99,
      "questionType": "SINGLE",
      "title": "你是否会经常急躁得坐立不安？",
      "group": 8,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    },
    {
      "questionId": 100,
      "questionType": "SINGLE",
      "title": "对不认识的人，你是否会都喜欢？",
      "group": 9,
      "children": [
        {
          "id": "A",
          "state": 0,
          "serial": "A",
          "content": "是",
          "score": 1
        },
        {
          "id": "B",
          "state": 0,
          "serial": "B",
          "content": "不是",
          "score": 0
        }
      ]
    }
  ]
}


// export const xiaoxue_json ={
// 	"pkId": "1",
// 	"title": "小学生心理健康评定量表（MHRSP）", 
// 	"desc": "<p>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>", 
// 	"number": 80, 
// 	"questions":[
// 	]
// }

export const xiaoxue_json ={
	"pkId": "1",
	"title": "小学生心理健康评定量表", 
	"desc": "<p>(1)这是一份有关小学生心理健康状况的评定量表。希望您能同我们密切合作，如实认真地加以评定。</p><p> (2)请您仔细阅读量表中的每一道题目，然后根据您对学生的日常观察、了解情况，选择相应的选项。</p>", 
	"number": 80, 
    "is_sum":false,
    "sum_key":'',
	"questions":[
  {
    "questionId": 1,
    "questionType": "SINGLE",
    "title": "不能正确认识字母或拼读音节。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 2,
    "questionType": "SINGLE",
    "title": "不能正确辨认汉字。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 3,
    "questionType": "SINGLE",
    "title": "不懂得数的大小和序列关系。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 4,
    "questionType": "SINGLE",
    "title": "计算困难。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 5,
    "questionType": "SINGLE",
    "title": "绘画时定位不准，涂色不合规范。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 6,
    "questionType": "SINGLE",
    "title": "图画作品中有前后左右位置颠倒的现象。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 7,
    "questionType": "SINGLE",
    "title": "一提起学习即心烦意乱。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 8,
    "questionType": "SINGLE",
    "title": "课堂讨论或与家长谈论学习问题时不感兴趣。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 9,
    "questionType": "SINGLE",
    "title": "不能按时交作业或作业质量差。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 10,
    "questionType": "SINGLE",
    "title": "考试不及格。",
    "group": 1,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 11,
    "questionType": "SINGLE",
    "title": "遇到一点小事也担忧。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 12,
    "questionType": "SINGLE",
    "title": "心神不定，坐立不安。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 13,
    "questionType": "SINGLE",
    "title": "食欲不振，心慌气促。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 14,
    "questionType": "SINGLE",
    "title": "头痛失眠汗多尿频。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 15,
    "questionType": "SINGLE",
    "title": "害怕上学，多方逃避。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 16,
    "questionType": "SINGLE",
    "title": "不敢独自出家门。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 17,
    "questionType": "SINGLE",
    "title": "一人独处时恐慌害怕。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 18,
    "questionType": "SINGLE",
    "title": "无缘无故地闷闷不乐。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 19,
    "questionType": "SINGLE",
    "title": "精力下降，活动减少。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 20,
    "questionType": "SINGLE",
    "title": "受到重大刺激不激动不流泪。",
    "group": 2,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 21,
    "questionType": "SINGLE",
    "title": "心胸狭窄，猜疑。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 22,
    "questionType": "SINGLE",
    "title": "依赖他人。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 23,
    "questionType": "SINGLE",
    "title": "嫉妒他人。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 24,
    "questionType": "SINGLE",
    "title": "胆怯，害羞。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 25,
    "questionType": "SINGLE",
    "title": "自卑自责。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 26,
    "questionType": "SINGLE",
    "title": "遇事犹豫不决。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 27,
    "questionType": "SINGLE",
    "title": "固执任性。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 28,
    "questionType": "SINGLE",
    "title": "容易发火。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 29,
    "questionType": "SINGLE",
    "title": "孤僻不合群。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 30,
    "questionType": "SINGLE",
    "title": "与人对立。",
    "group": 3,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 31,
    "questionType": "SINGLE",
    "title": "交新朋友困难。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 32,
    "questionType": "SINGLE",
    "title": "在集体场合适应困难。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 33,
    "questionType": "SINGLE",
    "title": "自我中心，不遵守集体规则。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 34,
    "questionType": "SINGLE",
    "title": "不能融洽地与同学相处。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 35,
    "questionType": "SINGLE",
    "title": "与教师或家长发生冲突。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 36,
    "questionType": "SINGLE",
    "title": "被别人误解后耿耿于怀。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 37,
    "questionType": "SINGLE",
    "title": "不能和常人一样地与异性交往。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 38,
    "questionType": "SINGLE",
    "title": "受到挫折后反应过分强烈或压抑。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 39,
    "questionType": "SINGLE",
    "title": "容易闯祸。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 40,
    "questionType": "SINGLE",
    "title": "面对新环境（迁居转学等）适应困难。",
    "group": 4,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 41,
    "questionType": "SINGLE",
    "title": "骂人。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 42,
    "questionType": "SINGLE",
    "title": "搞恶作剧。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 43,
    "questionType": "SINGLE",
    "title": "起哄，无理取闹。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 44,
    "questionType": "SINGLE",
    "title": "打架斗殴。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 45,
    "questionType": "SINGLE",
    "title": "故意破坏。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 46,
    "questionType": "SINGLE",
    "title": "考试作弊。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 47,
    "questionType": "SINGLE",
    "title": "说谎。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 48,
    "questionType": "SINGLE",
    "title": "偷盗。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 49,
    "questionType": "SINGLE",
    "title": "逃学。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 50,
    "questionType": "SINGLE",
    "title": "离家出走。",
    "group": 5,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 51,
    "questionType": "SINGLE",
    "title": "习惯性眨眼。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 52,
    "questionType": "SINGLE",
    "title": "习惯性皱眉或皱额。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 53,
    "questionType": "SINGLE",
    "title": "习惯性努嘴或嗅鼻。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 54,
    "questionType": "SINGLE",
    "title": "习惯性点头或摇头。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 55,
    "questionType": "SINGLE",
    "title": "习惯性吞咽或打呃。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 56,
    "questionType": "SINGLE",
    "title": "习惯性咳嗽。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 57,
    "questionType": "SINGLE",
    "title": "习惯性耸肩。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 58,
    "questionType": "SINGLE",
    "title": "吸吮手指嘴嚼衣服或其他物品。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 59,
    "questionType": "SINGLE",
    "title": "咬指甲。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 60,
    "questionType": "SINGLE",
    "title": "吸烟或饮酒。",
    "group": 6,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 61,
    "questionType": "SINGLE",
    "title": "反复数课本或其他图书上人物的数目。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 62,
    "questionType": "SINGLE",
    "title": "反复检查作业是否作对了。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 63,
    "questionType": "SINGLE",
    "title": "睡觉前反复检查个人的衣服鞋袜是否放整齐了。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 64,
    "questionType": "SINGLE",
    "title": "一天洗手十几次，每次持续十几分钟。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 65,
    "questionType": "SINGLE",
    "title": "注意力不集中，做事有头无尾。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 66,
    "questionType": "SINGLE",
    "title": "上课时小动作多，干扰他人。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 67,
    "questionType": "SINGLE",
    "title": "不合场合，特别好动。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 68,
    "questionType": "SINGLE",
    "title": "做作业时边做边玩。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 69,
    "questionType": "SINGLE",
    "title": "好冲动行动鲁莽。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 70,
    "questionType": "SINGLE",
    "title": "不知危险，好伤人或自伤。",
    "group": 7,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 71,
    "questionType": "SINGLE",
    "title": "尿床。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 72,
    "questionType": "SINGLE",
    "title": "口吃。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 73,
    "questionType": "SINGLE",
    "title": "好沉默不语，甚至长时间一言不发。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 74,
    "questionType": "SINGLE",
    "title": "入睡困难。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 75,
    "questionType": "SINGLE",
    "title": "睡觉不安稳，好讲梦话。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 76,
    "questionType": "SINGLE",
    "title": "睡觉时好磨牙。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 77,
    "questionType": "SINGLE",
    "title": "睡觉中突然哭喊惊叫。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 78,
    "questionType": "SINGLE",
    "title": "睡觉中突然起床活动，醒后对此无记忆。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 79,
    "questionType": "SINGLE",
    "title": "厌食偏食或拒食。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  },
  {
    "questionId": 80,
    "questionType": "SINGLE",
    "title": "身体无病却反复呕吐。",
    "group": 8,
    "children": [
      {
        "id": "A",
        "state": 0,
        "serial": "A",
        "content": "经常",
        "score": 2
      },
      {
        "id": "B",
        "state": 0,
        "serial": "B",
        "content": "偶尔",
        "score": 1
      },
      {
        "id": "C",
        "state": 0,
        "serial": "C",
        "content": "没有",
        "score": 0
      }
    ]
  }
]
} 
export const json_demo ={
  "pkId": "2",
  "title": "中学生心理健康诊断测验(MHT)",
  "desc": "<p>本测验共有100个项目，每个项目后面有“是”和“否”两个答案，请您根据自己的真实情况进行选择。</p> ",
  "number": 2,
  "is_sum":true,
  "questions":
  [
	   {
	      "questionId": 1,
	      "questionType": "SINGLE",
	      "title": "你夜里睡觉时，是否总想着明天的功课？",
	      "group": 1,
	      "children": [
	        {
	          "id": "A",
	          "state": 1,
	          "serial": "A",
	          "content": "是",
	          "score": 1
	        },
	        {
	          "id": "B",
	          "state": 0,
	          "serial": "B",
	          "content": "不是",
	          "score": 0
	        }
	      ]
	    },
	    {
	      "questionId": 2,
	      "questionType": "SINGLE",
	      "title": "老师在向全班提问时，你是否会觉得是在提问自己而感到不安？",
	      "group": 1,
	      "children": [
	        {
	          "id": "A",
	          "state": 1,
	          "serial": "A",
	          "content": "是",
	          "score": 1
	        },
	        {
	          "id": "B",
	          "state": 0,
	          "serial": "B",
	          "content": "不是",
	          "score": 0
	        }
	      ]
	    },
  ]
}

export const xiaoxue_report={
	"1":{
		"label":"学习障碍",
		"type":"维度一",
		"0_9":{ 
			state:"正常",
			result:"该学生在学习障碍因子上的得分较低，表示该学生不存在学习障碍方面的问题，主要表现为：在学习各个方面不存在明显的困难，学习时不会感到太吃力，能够按时完成老师布置的作业，对于普通的计算与认字等学习活动都能正常进行，考试一般能够及格，面对学习一般没有害怕、逃避或厌烦的情绪，家长不必为此担心。",
			advise:"该学生目前的状态很好，不存在学习障碍问题，建议老师提示家长继续为该学生营造良好的学习和成长环境，建立和维护好亲子关系。同时对于该学生在学习上的困惑能较好地进行有效指导，帮助该学生形成良好的学习习惯和科学的学习方法。"
		},
		"10_20":{
			state:"存在学习障碍",
			result:"该学生在学习障碍因子上的得分较高，表示该学生在智力方面无缺陷的前提下，学习方面可能存在一定的困扰或者障碍。可能表现为：成绩相对同龄人来讲比较差，考试经常不及格；在计算、识字或绘画等活动中存在一定的困难；一提学习就可能出现烦躁不安、逃避的现象；对于学习不感兴趣，难以按时提交作业等。",
			advise:"每个人的成长规律都不尽相同，或早或晚，或快或慢，一定的学习障碍不代表该学生一定在智力上存在问题。针对该学生目前的情况，建议老师加强和其父母的沟通，多给予该学生具体的指导和成长的空间，耐心引导并关注学生的变化，同时要懂得发掘和欣赏该学生的其他优点；提示家长为该学生提供健康温暖的成长环境，建立良好的亲子关系，可以经常和该学生一起参加活动或带孩子游玩，从中发现其兴趣及优势以更明确引导的方向。如果该学生的智力存在缺陷，也可以让家长带其求助于专业的心理咨询人员。"
		},
	},
	"2":{
		"label":"情绪障碍",
		"type":"维度二",
		"0_9":{ 
			state:"正常",
			result:"该学生在情绪障碍因子上的得分较低，表示该学生当前的情绪表现良好，不存在情绪障碍方面的问题，主要表现为：遇到问题时多数能积极应对，能够较好地调节自己的不良情绪；精力充沛，较有活力，睡眠和食欲良好，家长不必为此担忧。",
			advise:"该学生目前状态很好，不存在明显的情绪问题，建议老师鼓励该学生继续保持，提示家长在生活中注重为该学生营造温馨和积极的成长环境以及建立良好的亲子关系，给该学生一个情绪稳定的家庭氛围。"
		},
		"10_20":{
			state:"存在情绪障碍",
			result:"该学生在情绪障碍因子上的得分较高，表示该学生可能存在一定情绪困扰或障碍。可能表现为：常表现出恐惧、焦虑、抑郁等情绪；遇到一些小事也会担心紧张，不敢一个人独处；对父母的依赖较强；食欲不振，睡眠不好；害怕上学，总想法设法逃避；上课时总是心神不宁，坐立不安等。",
			advise:"情绪障碍会让该学生持续处于痛苦之中，希望老师能够与该学生耐心沟通，了解该学生内心的感受和需求，努力找到问题的根源，提示家长可以从以下几点进行调整：第一，改善夫妻之间的关系；第二，合理安排陪伴同该学生的时间；第三，调整教育该学生的方式。如果通过分析感觉调整比较困难，最好能及时寻求心理咨询师的专业帮助。"
		},
	},
	"3":{
		"label":"性格障碍",
		"type":"维度三",
		"0_9":{ 
			state:"正常",
			result:"该学生在性格障碍因子上的得分较低，表示该学生不存在性格障碍方面的问题，主要表现为：该学生既能很好地融入集体，与同龄人达成一片，也能适时保持安静与他人和平共处，您不必为此担心。",
			advise:"该学生的性格特点很好，建议老师鼓励该学生能继续保持，提示家长在生活中注重为该学生营造温馨和积极的成长环境以及建立良好的亲子关系。"
		},
		"10_20":{
			state:"存在性格障碍",
			result:"该学生在性格障碍因子上的得分较高，表示该学生在性格上存在异常倾向，可能表现为：固执任性，孤僻、不合群；过度依赖或嫉妒他人；容易自卑或自责；容易在与人相处时有较多的敌对情绪，也不易控制自己的情绪，经常为一些小事而冲人发脾气等。",
			advise:"性格是长时间形成的，也较易受后天环境的影响，因此也很难在短时间内发生巨大改变，建议老师加强和该学生家长的沟通，提示家长可以从以下几点进行调整：第一，改善夫妻之间的关系；第二，合理安排陪伴同该学生的时间；第三，调整教育该学生的方式，注重培养该学生乐观积极的性格特点，从该学生的行为习惯上对其进行纠正。如果通过分析感觉调整比较困难，最好能及时寻求心理咨询师的专业帮助。"
		},
	},
	"4":{
		"label":"社会适应障碍",
		"type":"维度四",
		"0_9":{ 
			state:"正常",
			result:"该学生在社会适应障碍因子上的得分较低，表示该学生适应良好，主要表现为：能够很好地融入学校环境，与其他同学建立良好的同伴关系，也能较好较快地适应新的班集体，能够遵守集体规则，与老师和家长相处顺利等。",
			advise:"该学生的社会适应能力很好，建议老师鼓励该学生能继续保持，提示家长在生活中注重为该学生营造温馨和积极的成长环境以及建立良好的亲子关系，在该学生进入一个新的环境时能够预先让该学生对环境有一个大致的了解。"
		},
		"10_20":{
			state:"存在适应障碍",
			result:"该学生在社会适应障碍因子上的得分较高，表示该学生存在一定的社会适应障碍倾向，可能表现为：常以自我为中心，不能很好地融入集体；在结交新朋友方面也存在困难；不能很顺利地融入或适应新环境；容易闯祸，受到挫折后反应过分强烈或压抑等。",
			advise:"社会适应障碍可能会让该学生内心感到孤独、焦虑，建议老师加强和该学生家长的沟通，提示家长可以从以下几点进行调整：第一，改善夫妻之间的关系；第二，合理安排陪伴同该学生的时间；第三，调整教育该学生的方式，注重培养该学生的独立性，教会该学生如何适应新环境，如果通过分析感觉调整比较困难，最好能及时寻求心理咨询师的专业帮助。"
		},
	},
	"5":{
		"label":"品德障碍",
		"type":"维度五",
		"0_9":{ 
			state:"正常",
			result:"该学生在品德障碍因子上的得分较低，表示该学生不存在品德障碍，主要表现为：该学生有着较好的自我行为约束，可遵守纪律和道德规范；能保持诚信的品质和行为习惯；注重文明礼貌，不无理取闹、骂人、打架等；也没有偷窃、说谎等不良行为，在学校的行为表现良好，您不必为此担忧。",
			advise:"该学生的行为品德很好，建议鼓励该学生能继续保持，提示家长在生活中注重为该学生营造温馨和积极的成长环境以及建立良好的亲子关系，并注重该学生品德方面的培养。"
		},
		"10_20":{
			state:"存在品德障碍",
			result:"该学生在品德障碍因子上的得分较高，表示该学生存在一定的品德问题倾向，可能表现为：常做出一些不符合道德和社会规范、纪律的行为，如故意破坏、说谎、偷窃、逃学等；不讲文明礼貌，经常出现骂人、打架的言语和行为；喜欢起哄和无理取闹等。",
			advise:"该学生的品德障碍会引发各种行为问题，这通常是内心冲突较多的表现。建议老师及时与该学生进行深入沟通交流，同时加强和该学生家长的沟通，提示家长可以从以下几点进行调整：第一，改善夫妻之间的关系；第二，合理安排陪伴同该学生的时间；第三，调整教育该学生的方式，从日常生活的小事对该学生的不良行为习惯进行纠正。如果通过分析感觉调整比较困难，最好能及时寻求心理咨询师的专业帮助。"
		},
	},
	"6":{
		"label":"不良习惯",
		"type":"维度六",
		"0_9":{ 
			state:"正常",
			result:"该学生在不良习惯因子上的得分较低，表示该学生没有不良习惯。主要表现为：在日常生活中的行为表现良好，没有习惯性的咳嗽、耸肩、点头或摇头等动作，并不存在严重的不良行为习惯。",
			advise:"该学生的行为习惯很好，建议老师鼓励该学生能继续保持，提示家长在生活中注重为该学生营造温馨和积极的成长环境以及建立良好的亲子关系，在日常生活中为该学生树立良好的行为榜样。"
		},
		"10_20":{
			state:"存在不良习",
			result:"该学生在不良习惯因子上的得分较高，表示该学生可能存在一定的不良习惯。可能表现为：在日常生活中可能存在一些习惯性的不良行为，如：习惯性眨眼、耸肩、吮吸手指、咳嗽、咬指甲，甚至有吸烟喝酒等不良行为出现",
			advise:"该学生的某些不良习惯是暂时性的，会随着年龄的增长而逐渐消失，但是如果较为严重而持续时间较长，则提示可能会出现一定的行为问题，建议老师加强和该学生家长的沟通，提示家长可以带其求助于专业的心理咨询或心理治疗工作人员"
		},
	},
	"7":{
		"label":"行为障碍",
		"type":"维度七",
		"0_9":{ 
			state:"正常",
			result:"该学生在行为障碍因子上的得分较低，表示该学生不存在行为障碍，您不必在此方面过分担忧。主要表现为：该学生的大多数行为与同龄人表现一样，没有强迫性的动作或行为出现，上课时表现较好，能较好地遵守课堂纪律；做作业时能较为专心，做事不冲动等。",
			advise:"该学生的行为习惯很好，没有行为障碍问题，建议老师鼓励该学生能继续保持，提示家长在生活中注重为该学生营造温馨和积极的成长环境以及建立良好的亲子关系，对于该学生良好的行为习惯给予一定的奖励或者表扬。"
		},
		"10_20":{
			state:"存在行为障碍",
			result:"该学生在行为障碍因子上的得分较高，表示该学生可能存在一定程度的行为障碍，可能表现为：注意力不集中，好冲动、小动作多、好动；做事不分场合，上课时不爱听讲，喜欢干扰他人；做作业边做边玩等。",
			advise:"该学生的行为障碍问题可轻可重，无法进行简单地诊断，建议老师加强和该学生家长的沟通，提示家长带其到心理治疗机构或医院精神科做进一步检查，早日找到问题根源，若是较为严重则可建议家长带该学生接受专业的心理治疗。"
		},
	},
	"8":{
		"label":"特种障碍",
		"type":"维度八",
		"0_9":{ 
			state:"正常",
			result:"该学生在特种障碍因子上的得分较低，表示该学生不存在特种障碍，主要表现为：该学生的心身健康状况良好，在饮食、睡眠等生理活动均很顺利，与一般孩子无异。",
			advise:"建议老师鼓励该学生能继续保持目前的状态，提示家长在生活中注重为该学生营造温馨和积极的成长环境以及建立良好的亲子关系，注意该学生正常的作息与饮食。"
		},
		"10_20":{
			state:"存在特种障碍",
			result:"该学生在特种障碍因子上的得分较高，表示该学生可能存在着某种特定的心身障碍，可能表现为：入睡较为困难，睡觉时好磨牙、讲梦话、哭喊、惊叫等；不喜欢说话，总保持沉默；频繁性尿床；在饮食方面习惯不好，经常有厌食、限食或拒食的表现等。",
			advise:"针对该学生目前的状况，建议老师加强和该学生家长的沟通，提示家长带该学生到医院做全面的身体健康检查，若确实存在身体健康问题，可以向相应医生问诊并对该学生的健康进行改善；若可以排除器质性病变，可以建议家长求助于心理咨询人员或到医院精神科进行进一步诊断和相应的治疗。"
		},
	},
}


export const chuzhong_report={
	"11":{
		"label":"学习焦虑",
		"type":"维度一",
		"0_3":{ 
			state:"焦虑程度较低",
			result:"您在学习焦虑维度上的得分较低，表明您的学习焦虑水平处于低分等级，主要表现为:您的学习很少会受到困扰，能正确对待考试成绩；在临近考试前有较平常的心态，沉着应对，既不过分紧张也不过分疏忽；当考试成绩不好时能积极主动寻找原因并加以改进，而非一味地担忧或害怕考试等。",
			advise:"您能较好地处理学习中遇到的困扰，建议您继续保持积极乐观的学习态度和处理困难的方式；另外也需要对焦虑有一个正确的理解，合理的焦虑情绪会有助于学习效率的提高，当出现一些紧张害怕的感受时，可以自我提醒这是正常现象，也可以运用一些方法自我调整，例如可以尝试学习进行自我放松的方法，培养一些兴趣爱好，积极暗示自己等。"
		},
		"4_7":{ 
			state:"焦虑程度一般",
			result:"您在学习焦虑维度上的得分处于中间状态，表明您的学习焦虑水平处于中间等级，可能表现为：您相对会关注考试分数，分数较低时，对学习有一些担心和紧张；在考试来临前有一定的担忧和紧张，但没有害怕心理；对学习的担心程度和一般人差不多等。",
			advise:"针对您的状态，建议您可以制定适合自己的学习计划和目标，对自己擅长和不擅长的知识点分别进行总结，为下一阶段的学习打基础；其次调节自己的心态，如果成绩不理想，可以向老师求助，帮助自己制定适合的计划，也可以主动多培养一些兴趣、爱好帮助其提升自信，可以尽快走出紧张状态，稳步提升。"
		},
		"8_15":{ 
			state:"焦虑程度较高",
			result:"您在学习焦虑维度上的得分较高，表明您的学习焦虑水平处于较高等级，可能表现为：您对考试怀有恐惧心理，无法安心学习，十分关心考试分数；晚上可能因对学习的担心而无法入睡；一到考试临近就非常紧张，很怕自己会考不好；当着大家的面朗读课文或回答问题时也总会感到紧张不安，担心自己表现不好。",
			advise:"针对您的状态，建议您可以和老师或父母及时沟通内心的想法，让他们根据自己的状态制定适合的学习计划以及目标，不要给自己太高的期望和要求，坚信只要坚持、有计划，日积月累，就能够成功；其次，快乐心情也非常重要；课外时间可以多培养一些兴趣、爱好来调节紧张的学习，往往可能带来更明显的进步。"
		}
	},
	"1":{
		"label":"学习焦虑",
		"type":"维度一",
		"0_3":{ 
			state:"焦虑程度较低",
			result:"您在学习焦虑维度上的得分较低，表明您的学习焦虑水平处于低分等级，主要表现为:您的学习很少会受到困扰，能正确对待考试成绩；在临近考试前有较平常的心态，沉着应对，既不过分紧张也不过分疏忽；当考试成绩不好时能积极主动寻找原因并加以改进，而非一味地担忧或害怕考试等。",
			advise:"您能较好地处理学习中遇到的困扰，建议您继续保持积极乐观的学习态度和处理困难的方式；另外也需要对焦虑有一个正确的理解，合理的焦虑情绪会有助于学习效率的提高，当出现一些紧张害怕的感受时，可以自我提醒这是正常现象，也可以运用一些方法自我调整，例如可以尝试学习进行自我放松的方法，培养一些兴趣爱好，积极暗示自己等。"
		},
		"4_7":{ 
			state:"焦虑程度一般",
			result:"您在学习焦虑维度上的得分处于中间状态，表明您的学习焦虑水平处于中间等级，可能表现为：您相对会关注考试分数，分数较低时，对学习有一些担心和紧张；在考试来临前有一定的担忧和紧张，但没有害怕心理；对学习的担心程度和一般人差不多等。",
			advise:"针对您的状态，建议您可以制定适合自己的学习计划和目标，对自己擅长和不擅长的知识点分别进行总结，为下一阶段的学习打基础；其次调节自己的心态，如果成绩不理想，可以向老师求助，帮助自己制定适合的计划，也可以主动多培养一些兴趣、爱好帮助其提升自信，可以尽快走出紧张状态，稳步提升。"
		},
		"8_15":{ 
			state:"焦虑程度较高",
			result:"您在学习焦虑维度上的得分较高，表明您的学习焦虑水平处于较高等级，可能表现为：您对考试怀有恐惧心理，无法安心学习，十分关心考试分数；晚上可能因对学习的担心而无法入睡；一到考试临近就非常紧张，很怕自己会考不好；当着大家的面朗读课文或回答问题时也总会感到紧张不安，担心自己表现不好。",
			advise:"针对您的状态，建议您可以和老师或父母及时沟通内心的想法，让他们根据自己的状态制定适合的学习计划以及目标，不要给自己太高的期望和要求，坚信只要坚持、有计划，日积月累，就能够成功；其次，快乐心情也非常重要；课外时间可以多培养一些兴趣、爱好来调节紧张的学习，往往可能带来更明显的进步。"
		}
	},
	"2":{
		"label":"社交焦虑",
		"type":"维度二",
		"0_3":{ 
			state:"焦虑程度较低",
			result:"您在社交焦虑维度上的得分较低，表明您的社交焦虑水平处于较低等级，主要表现为：您在对人方面能够做到热情大方，并容易结交朋友；与人相处时能顺畅、流利和人沟通，自然表达自己的观点和想法；在和同学交谈的过程中总是积极参与，给人的印象较好，人缘很好。",
			advise:"存在社交焦虑的个体，不善于与人接触，从而影响人际交往，在面对困难和失败时缺少一定的支持，而没有社交焦虑的个体一般人际关系情况较好。您在社交焦虑维度上处于良好的状态，可以自在的和他人交流，不会感到紧张，这样的状态很好，希望能够继续保持。"
		},
		"4_7":{ 
			state:"焦虑程度一般",
			result:"您在社交焦虑维度上的得分处于中间状态，表明您的社交焦虑水平处于中间等级，可能表现为：您在熟悉的人面前可以做到正常交流，有一些要好的朋友；但如果是在陌生人较多的场合或自己不熟悉的场合，可能会有一定程度的紧张和表现不自然。",
			advise:"存在社交焦虑的个体，不善于与人接触，从而影响人际交往，在面对困难和失败时缺少一定的支持。在与熟人正常交流的同时建议您可以尝试扩展自己的朋友圈范围，尝试和不同的人进行交流，要相信每个人都有和自己相同的地方，学习欣赏他人的优点，减少对自己形象的关注，在尝试中增加自信和提升交往能力。"
		},
		"8_15":{
			state:"焦虑程度较高",
			result:"您在社交焦虑维度上的得分较高，表明您的社交焦虑水平较高，可能表现为：您平常过分注重自己的形象，害怕与人交往，退缩；在人多的场合总表现为退缩和沉默，不善言辞；不喜欢参加各种活动，觉得和人一起时自己是个多余的人；与人交往时总会感到紧张。",
			advise:"严重的社交焦虑能够影响正常的人际交往，每个人都有自己独特的地方，我们可以想象每个人身上都有很多门，可以尝试像玩游戏一样打开不同的大门，看看里面都有什么样新奇的内容，也看看别人和自己的有什么区别，思考应该怎么样和谐地与人相处，将相处看得简单一些，那么自然就不会紧张担心了；也可以尝试将自己的担心向好朋友说，这也有助于减缓自己紧张的心情，提高交流的能力。"
		},
	},
	"3":{
		"label":"孤独倾向",
		"type":"维度三",
		"0_3":{ 
			state:"孤独倾向较弱",
			result:"您在孤独倾向维度上的得分较低，表明您的孤独倾向较弱，主要表现为：您的乐群性较高，愿意与人交往，敞开心扉；日常生活中拥有良好的人际关系，有亲密、可靠的朋友，同家人相处得也很好，在集体中或与周围的人也相处得很融洽，几乎没有孤独的感觉。",
			advise:"良好的人际关系是强大的支持系统，您目前在与人相处的状态以及性格上的积极态度非常好，建议您继续保持这种良好的状态。"
		},
		"4_7":{ 
			state:"孤独倾向一般",
			result:"您在孤独倾向维度上的得分处于中间状态，表明您的孤独倾向一般。可能表现为：您的乐群性一般，在熟人面前可以敞开心扉，和家人相处良好，有个别比较亲密的朋友，但在集体中或周围人相处中也会有一些孤独感，不太愿意述说自己的真实想法。",
			advise:"良好的人际关系是强大的支持系统，建议在维持目前的交流状态下，可以尝试扩展自己的交流范围，如参加一些活动或培养自己的兴趣，认识更多的朋友，愿意和更多的人沟通，分享内心的想法，有助于更快乐的学习和成长。"
		},
		"8_15":{
			state:"孤独倾向较强",
			result:"您在孤独倾向维度上的得分较高，表明您可能存在较为强烈的孤独感，可能表现为：没有什么亲密的朋友，同家人沟通也较少，在集体中几乎没有归属感；您常常感受到很苦闷，心里话不知该找谁诉说，很可能经常写日记并把它当成自己心灵的朋友。",
			advise:"针对您目前的状态，可以尝试从以下几个方面进行改善：首先，您应该勇敢地打开心灵的门窗，走出个人小天地，积极参与学校组织的活动；其次，尽可能地培养起良好的兴趣爱好或参加一些公益活动，引发新的目标；第三，可以和父母或老师沟通，给您适当变换环境，随环境的变化而变换自己的心情，使自己始终保持健康向上的心理。"
		},
	},
	"4":{
		"label":"自责倾向",
		"type":"维度四",
		"0_3":{ 
			state:"自责倾向较弱",
			result:"您在自责倾向维度上的得分较低，表明您的自责倾向较弱，主要表现为：您比较自信，能够较为正确地看待失败，多数情况下能客观分析原因，并能适当地把失败归为外部原因，如考试成绩不理想时不一味责备自己，认为可能与考试难度等因素也有关。",
			advise:"自责可能是“有用”的，它能帮助我们认识自己、改变行为；但自责也可能是“有害”的，它可能是一种应激障碍典型的症状。建议您在保持目前状态的同时，也要形成一种积极的心态，就是保持一种“不完美的自己正在变得越来越好”的心态，自责就会化消极为积极。"
		},
		"4_7":{ 
			state:"自责倾向一般",
			result:"您在自责倾向维度上的得分处于中间状态，表明您的自责倾向处于中等水平。可能表现为：当您遇到一些挫折和困难时，有时可能会觉得是自己做得不够好，但不会过于自责，可以通过自己的努力进行调节，走出困扰。",
			advise:"自责可能是“有用”的，它能帮助我们认识自己、改变行为；但自责也可能是“有害”的，它可能是一种应激障碍典型的症状。建议您能对失败和挫折进行合理归因，当遇到挫折或失败时学会调整自己的不合理认知；制定合理的目标以及调整对自己的期望，使结果不过分超出预期。"
		},
		"8_15":{
			state:"自责倾向较强",
			result:"您在自责倾向维度上的得分较高，表明您的自责倾向较强，可能现为：您常怀疑自己的能力，对自己不自信，常将失败、过失归咎于自己；当发生不如意的事情时，不认为是他人的问题，而经常认为是自己不好，对自己所做的事抱有恐惧倾向。",
			advise:"自责在一定程度上是“有用”的，它能帮助我们认识自己从而改变行为；但自责也可能是“有害”的，它可能是一种应激障碍典型的症状。建议您能转变对失败的认知归因，适当进行外归因，在您犯错或是事情进展得不顺利时不过分自责；可以给自己积极的自我暗示，平常注意提升您的自信心和理性分析问题的能力；如果效果不太明显，建议及时带您咨询专业的心理咨询师，制定适合的调整方案。"
		},
	},
	"5":{
		"label":"过敏倾向",
		"type":"维度五",
		"0_3":{ 
			state:"过敏倾向较弱",
			result:"您在过敏倾向维度上的得分较低，表明您对事物或人没有明显的过敏反应。主要表现为：能够较果断地处理日常事物，并较少受到周围事物的影响；对于同学和老师对自己的评价能较客观地接纳，不会过分在意他人对自己的看法。",
			advise:"您的过敏倾向比较低，属于较粗线条的心理感受，从一方面讲是一种良好的心理状态，因为可以有效抵抗外界的干扰因素；但另一方面，也会造成感受性的降低，对外界事物不敏感，应注意把握二者之间的平衡。"
		},
		"4_7":{ 
			state:"过敏倾向一般",
			result:"您在过敏倾向维度上的得处于中间状态，表明您对人或事的敏感性一般，可能表现为：您对自己比较重视的事情会比较敏感，其他事情对您的干扰相对较小，基本能够独立处理日常生活事物，有时可能存在一定的犹豫不决的情况。",
			advise:"针对您目前的状态，除了避免无关事情对您的影响，建议您在担心自己重视的事情时，可以找愿意倾听并能够很好理解您的人进行诉说，比如很好的朋友、喜欢的老师或关心自己的家人等，他们的理解能够减轻您内心的困扰。"
		},
		"8_15":{
			state:"过敏倾向较强",
			result:"您在过敏倾向维度上的得分较高，表明您对人或事的敏感性较高，可能表现为：您性格较为内向，对一些事情过于敏感，容易为一些小事而烦恼；对身边发生过的事过于介意，一有什么事就放心不下，常感到担心与不安；有时同学或老师一句无意的话也会思量许久并耿耿于怀。",
			advise:"过分敏感会使人在与人交往的过程中常小心翼翼并紧张不安。针对您目前的状况，在日常生活中，建议您可以找愿意倾听并能够很好理解您的人进行诉说，比如很好的朋友、喜欢的老师或关心自己的家人等，他们的理解能够减轻您内心的困扰。如果症状持续的时间比较长，对您正常的生活或学习造成了影响，建议您接受专业的心理疏导。"
		},
	},
	"6":{
		"label":"身体症状",
		"type":"维度六",
		"0_3":{ 
			state:"身体状况正常",
			result:"您在身体特征维度上的得分较低，表明您基本没有身体异常表现。主要表现为：无呼吸困难，心跳加快或心神不定、食欲不振等状况，饮食和睡眠等都正常。身体状况正常",
			advise:"当面临突如其来的情绪困扰时，您能保持一种良好的身体反应，这是一种良好的现象，希望您继续保持这种良好的抵抗力。"
		},
		"4_7":{ 
			state:"身体状况一般",
			result:"您在身体特征维度上的得分处于中间状态，表明您在焦虑状态会出现不太明显的身体症状。可能表现为：在一定的紧张情况下（如当众回答问题或发言时）偶尔会出现呼吸困难，心跳加快、手心出汗或心神不定、食欲不振等症状，但不会影响您的正常表现。",
			advise:"当面临突如其来的情绪困扰时，您会产生一些身体上的不适反应，长此以往会对身体健康造成一定的损伤，建议您及时和父母或老师沟通，让他们帮助您制定一些指导计划，学习一些常用的放松技术训练，如呼吸调节、肌肉放松训练、自主调节训练法等。"
		},
		"8_15":{
			state:"身体症状明显",
			result:"您在身体特征维度上的得分较高，表明您在极度焦虑的时候，会出现较为明显的身体症状。可能表现为：常会出现呕吐失眠、小便失禁等明显症状；如果焦虑是慢慢产生的，就会心神不定，心跳异常，脉搏混乱，想呕吐，食欲不振，肚子痛或失眠等。",
			advise:"当面临情绪困扰时，您会产生一些身体上的不适反应，长此以往会对身体健康造成一定的损伤，建议您及时和父母或老师沟通，让他们帮助您制定一些指导计划，学习一些常用的放松技术训练，如呼吸调节、肌肉放松训练、自主调节训练法等。如果效果不明显，可以及时求助专业的心理咨询机构帮助您一起调整。"
		},
	},
	"7":{
		"label":"恐怖倾向",
		"type":"维度七",
		"0_3":{ 
			state:"恐怖倾向较弱",
			result:"您在恐怖倾向维度上的得分较低，表明您基本没有恐怖感，例如恐高、恐黑等，与其他您的表现一样，能较好地面对一些在正常范围内相对较为危险的情况。",
			advise:"您在恐怖倾向的维度上表现良好，没有特别害怕的地点或事物，能够正确地看待，建议您在日常生活中稍加注意，养成良好的生活习惯即可。"
		},
		"4_7":{ 
			state:"恐怖倾向一般",
			result:"您在恐怖倾向维度上的得分处于中间状态，表明您可能存在轻微的恐怖倾向，但没有影响您的正常学习和生活。可能表现为：对某些日常事物，如黑暗和高度等，会有一定的恐怖感，但在有人陪伴的情况下能克服这种恐惧。建议您平时注意和朋友及家人多沟通交流，帮助您合理地看待和分析自己担心的事情；也多参加户外集体活动，通过对事物的判断力；同时也要平常注意多休息，养成好的生活习惯。",
			advise:"建议您平时注意和朋友及家人多沟通交流，帮助您合理地看待和分析自己担心的事情；也多参加户外集体活动，通过对事物的判断力；同时也要平常注意多休息，养成好的生活习惯。"
		},
		"8_15":{
			state:"恐怖倾向较强",
			result:"您在恐怖倾向维度上的得分较高，表明您可能存在较为明显的恐怖倾向。可能表现为：会害怕很多东西，对某些日常事物如黑暗、高度等有较严重的恐怖感，晚上需要开着灯睡觉，一旦打雷就感到很害怕，不敢一个人待在屋里等。",
			advise:"建议您平时注意和朋友及家人多沟通交流，帮助您合理地看待和分析自己担心的事情；也多参加户外活动，增加对事物的判断力；平常注意多休息，养成好的生活习惯。如果感觉效果不明显，而且对正常学习和生活造成了影响，建议让父母带您咨询专业的心理老师，帮助您及时分析和调整。"
		},
	},
	"8":{
		"label":"冲动倾向",
		"type":"维度八",
		"0_3":{ 
			state:"冲动倾向较弱",
			result:"您在冲动倾向维度上的得分较低，表明您基本没有冲动倾向。主要表现为：不会无缘无故地想哭，或者一看到想要的东西，就一定要拿到手，毫无理由地想到远处去，或想死等，遇事时较为冷静，有一定的忍耐性，能以较为平静的心态处理和面对。",
			advise:"您在冲动倾向维度上的得分处于良好状态，不存在冲动倾向带来的困扰。希望您继续保持这种良好的状态，但若存在情绪困扰应找到积极的发泄方式。"
		},
		"4_7":{ 
			state:"冲动倾向一般",
			result:"您在冲动倾向维度上的得分处于中间状态，表明您的冲动倾向一般。可能表现为：在日常生活中偶尔会有一些冲动行为，如偶尔会无缘无故地想哭，或者一看到想要的东西，就一定要拿到手，毫无理由地想到远处去，或想死等。",
			advise:"造成冲动倾向的原因有很多，如遗传因素、强烈焦虑情绪的驱使、后天的学习与模仿、冲动行为的不恰当强化等。针对您的状态，应及时和父母或老师沟通，让他们帮助您分清造成冲动倾向的原因，采取相应的方法进行改善。"
		},
		"8_15":{
			state:"冲动倾向较强",
			result:"您在冲动倾向维度上的得分较高，表明您的自制力较差，经常会有冲动行为。可能表现为：内部具有一定的焦虑倾向；严重时表现为无缘无故地想哭，或者一看到想要的东西，就一定要拿到手，毫无理由地想到远处去，或想死等。造成冲动倾向的原因有很多，如遗传因素、强烈焦虑情绪的驱使、后天的学习与模仿、冲动行为的不恰当强化等，严重的冲动倾向会使您的个性易走向极端。针对您目前的状况，针对您的状态，应及时和父母或老师沟通，让他们帮助您分清造成冲动倾向的原因，采取相应的方法进行改善。如果感觉效果不明显，也可以咨询专业的心理咨询师帮助您调整。",
			advise:"造成冲动倾向的原因有很多，如遗传因素、强烈焦虑情绪的驱使、后天的学习与模仿、冲动行为的不恰当强化等，严重的冲动倾向会使您的个性易走向极端。针对您目前的状况，针对您的状态，应及时和父母或老师沟通，让他们帮助您分清造成冲动倾向的原因，采取相应的方法进行改善。如果感觉效果不明显，也可以咨询专业的心理咨询师帮助您调整。"
		},
	},
	"9":{
		"label":"效度量表",
		"type":"维度九",
		"0_3":{ 
			state:"有参考价值",
			result:"您在效度量表上的得分处于较低水平，可以认为您对问卷做了较为真实地回答，作答结果具有一定的参考性。",
			advise:"希望继续保持认真答题的态度。"
		},
		"4_7":{ 
			state:"有参考价值",
			result:"您在效度量表上的得分处于较低水平，可以认为您对问卷做了较为真实地回答，作答结果具有一定的参考性。希望继续保持认真答题的态度。",
			advise:"希望继续保持认真答题的态度。"
		},
		"8_15":{
			state:"真实性不大",
			result:"您在效度量表上的得分处于较高水平，表明您对自己不利的评价进行了一定的掩饰，但也有可能是由于自身定位不准确，自我认识不清或理解能力不足造成的。",
			advise:"建议根据自己真实情况认真作答。"
		},
	},
	"10":{
		"label":" 总评",
		"type":"维度十",
		"0_55":{ 
			state:"正常",
			result:"您的得分较低，表明您的心理健康状况基本正常，主要表现为:您和身边人的关系融洽，能和周围人良好的沟通，学习很少会受到困扰，能正确对待考试成绩；身体健康状况良好，生活状态比较放松，自如。",
			advise:"您能较好地处理生活中遇到的困扰，可以积极地面对生活，建议您继续保持积极乐观的生活态度。"
		},
		"56_64":{ 
			state:"心理状态欠佳",
			result:"您的得分处于中间状态，表明您的心理状态欠佳，可能表现为：在学习活动中会出现一定的焦虑不安情绪，可能会伴有一定的冲动行为等，在与人交往过程中的有时会比较不自信，从而产生焦虑的情绪，和人沟通时容易紧张，整体心理健康状况有待改善。",
			advise:"您当前心理状况欠佳，针对您的状态，建议您可以培养一些兴趣、爱好帮助自己提升自信，走出紧张状态，在人际交往方面，可以尝试扩展自己的朋友圈范围，尝试和不同的人进行交流，要相信每个人都有和自己相同的地方，学习欣赏他人的优点，提高自己的交际能力，尽快改善自己当前的状态，提高自己的心理健康水平。"
		},
		"65_100":{
			state:"心理状态较差",
			result:"您的得分较高，表明您当前心理状态较差，可能存在心理问题，具体表现为：在学习活动中会出现一定的焦虑不安情绪，可能会伴有一定的冲动行为等，在与人交往过程中的焦虑情绪，神经敏感度较高，对于生活中出现的小事比较敏感，可能伴有一定的躯体症状，同时伴有恐高、恐黑等莫名的恐惧心理。",
			advise:"您当前心理状态较差，针对您的状态，建议您可以和老师或父母及时沟通内心的想法，表达自己的焦虑情绪及面对的问题，请求他们的帮助，必要时，还可以去求助医生或者心理咨询师等专业人士，对于自己的问题进行进一步的筛查，确定具体问题。"
		},
	}
}


